oh-my-posh init pwsh | Invoke-Expression
Import-Module posh-git
$env:POSH_GIT_ENABLED = $true
oh-my-posh init pwsh --config "$env:POSH_THEMES_PATH\powerline.omp.json" | Invoke-Expression

Import-Module -Name Terminal-Icons

# PSReadLine
Set-PSReadLineOption -BellStyle None
Set-PSReadLineKeyHandler -Chord 'Ctrl+d' -Function DeleteChar
Set-PSReadLineOption -PredictionSource History

# Fzf
Import-Module PSFzf
Set-PsFzfOption -PSReadlineChordProvider 'Ctrl+f' -PSReadlineChordReverseHistory 'Ctrl+r'

# Alias
Set-Alias ll ls
Set-Alias g git

# Utilities
function which ($command) {
  Get-Command -Name $command -ErrorAction SilentlyContinue |
    Select-Object -ExpandProperty Path -ErrorAction SilentlyContinue
}

function gccd ($url) {
  $name = $url.Split('/')[-1].Replace('.git', '')
  & git clone $url $name | Out-Null
  Set-Location $name
}

function mkcd () {
  param (
    [String]$Path
  )
  New-Item -ItemType Directory $Path
  Set-Location $Path
}